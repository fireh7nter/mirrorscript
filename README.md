# mirrorScript
Script to change official Kali repository to mirrors. This helps increase packages update and downloading for some user.

## Requirements
Kali Linux,
Python2.7 (comes pre-bundled)

## Usage
Run the script in privilege mode, such that `sources.list` could be edited.
